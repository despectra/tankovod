$IMPORTS$
from core.GamePrinter import GamePrinter
from core.config import screen_width, screen_height, match_duration


def main():
    game = GamePrinter(screen_width,
                       screen_height,
                       match_name="$MATCH_NAME$",
                       match_duration=match_duration,
                       out_file="$OUT_FILE$")

    $PLAYERS_INIT$

    game.run()


if __name__ == "__main__":
    main()
