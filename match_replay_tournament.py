import argparse
import sys

from core.Game import Game


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('tournament_file')
    args = parser.parse_args(sys.argv[1:])

    with open(args.tournament_file) as tournament_file:
        matches = [line.rstrip() for line in tournament_file]

    if len(matches) == 0:
        print('No matches in tournament. Exit')
        exit(0)

    game = Game.create(handle_continue_button=True)
    for match_file in matches:
        game.setup_new_single_replay(replay_file=match_file)
        game.run()
        game.reset()


if __name__ == "__main__":
    main()
