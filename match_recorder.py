import argparse
import subprocess
from datetime import datetime
import json
import sys

from core.config import tank_skins, screen_width, screen_height, player_init_locations_multipliers, player_colors, \
    player_init_angles

match_script_template_path = 'offline_main_template.pyt'
match_script_path = 'match_script.py'


def make_imports(players):
    import_set = set()
    for player in players:
        bot = player['bot']
        import_set.add(f'from bots.{bot} import {bot}')
    return '\n'.join(import_set)


def make_players_init_lines(players):
    lines = []
    player_varnames = []
    for i, player in enumerate(players):
        (x_mult, y_mult) = player_init_locations_multipliers[i]
        skin = tank_skins[player_colors[i]]
        angle = player_init_angles[i]
        speed = 0
        x = x_mult * screen_width / 4
        y = y_mult * screen_height / 4

        num = i + 1
        name = player['name']
        bot = player['bot']

        player_var_name = f'player{num}'
        player_varnames.append(player_var_name)

        lines.append(f'{player_var_name} = {bot}("{name}")')
        lines.append(f'{player_var_name}.setup(game, {x}, {y}, {angle}, {speed}, "{skin}", is_ui=False)')

    lines.append(f'game.set_players(players=[{", ".join(player_varnames)}])')
    return lines


def make_match_script(match, match_record_file):
    name = match['name']
    players = match['players']

    imports = make_imports(players)
    players_init_lines = make_players_init_lines(players)
    with (
        open(match_script_template_path, mode='r') as template_file,
        open(match_script_path, mode='w') as script_file
    ):
        for line in template_file:
            if '$PLAYERS_INIT$' in line:
                for init_line in players_init_lines:
                    script_file.write(line.replace('$PLAYERS_INIT$', init_line))
            else:
                rendered_line = line.replace('$IMPORTS$', imports)\
                    .replace('$MATCH_NAME$', name)\
                    .replace('$OUT_FILE$', match_record_file)
                script_file.write(rendered_line)


def add_match_to_tournament(tournament_path, match_file_path):
    with open(tournament_path, mode='a') as tournament_file:
        tournament_file.write(f'{match_file_path}\n')


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--config', help='Matches configuration file', required=True)
    parser.add_argument('-o', '--output', help='Tournament file to store paths of the recorded matches', required=True)
    parser.add_argument('-m', '--match', help='Name of the single match to record. If omitted then each match from '
                                              'config will be recorded')
    args = parser.parse_args(sys.argv[1:])

    with open(args.config) as config_file:
        config = json.load(config_file)
        matches = []
        match_key = args.match

        if match_key is None:
            for key, match_descr in config.items():
                matches.append(match_descr | {'key': key})
        else:
            match = config[match_key]
            if match is None:
                print(f'match {args.match} not found in config')
                exit(1)
            matches.append(match | {'key': match_key})

        for match in matches:
            match_name = match['name']
            match_key = match['key']
            print(f'Recording match {match_name}')
            now = datetime.now()
            match_file_path = f'out/match_{match_key}_{now.strftime("%Y-%m-%d_%H%M%S")}.json'
            make_match_script(match, match_file_path)
            record_process = subprocess.run(['python3', match_script_path])
            if record_process.returncode is 0:
                add_match_to_tournament(args.output, match_file_path)
            else:
                print(f'Match {match_name} recording failed')


if __name__ == '__main__':
    main()
