import argparse
import sys

from core.Game import Game


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('replay_file')
    args = parser.parse_args(sys.argv[1:])

    game = Game.create()
    game.setup_new_single_replay(replay_file=args.replay_file)

    game.run()


if __name__ == "__main__":
    main()
