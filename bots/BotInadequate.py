import random
from core.BasePlayer import BasePlayer


class BotInadequate(BasePlayer):
    def update(self, tick):

        self.rotate_by_angle(90)

        if random.random() < 0.03:
            if self.has_blast:
                self.shoot_blast()
            else:
                self.shoot_bullet()

        if random.random() < 0.05:
            self.speed_up()
