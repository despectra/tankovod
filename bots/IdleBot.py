from core.BasePlayer import BasePlayer
import random


class IdleBot(BasePlayer):

    def update(self, tick):
        target_enemy = random.choice(self.enemies)
        angle_to_enemy = self.angle_to_object(target_enemy)
        self.rotate_by_angle(angle_to_enemy)
        if abs(angle_to_enemy) < 5:
            self.shoot_bullet()
