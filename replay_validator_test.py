from core.replay.data.GameObjectState import GameObjectState
from core.replay.data.PlayerResultData import PlayerResultData
from core.replay.data.PlayerState import PlayerState
from core.replay.ReplayReader import ReplayReader


def print_player_result(player: PlayerResultData):
    print(f'\t\t{player.name}: SCORE {player.score}, HP {player.health} {"WINNER" if player.is_winner else ""}')


def print_player(player: PlayerState):
    print(f'\t{player.name}: [{player.x}; {player.y}]; {player.angle} deg.; SCORE {player.score}, HP {player.health}')


def print_obj(_id: str, obj: GameObjectState):
    print(f'\t{_id}:   {obj.obj_type}: [{obj.x}; {obj.y}]; {obj.angle} deg.')


def main():
    replay_data = ReplayReader.read(replay_file='out/offline_match.json')
    print('Replay data:')
    print(f'World: {replay_data.world.width} x {replay_data.world.height}, terrain type {replay_data.world.terrain}')
    print(f'Match name: {replay_data.match.name}')
    print(f'Duration: {replay_data.match.duration}')
    print(f'Frames count: {replay_data.frames_count}')
    print(f'Results:')
    for player_result in replay_data.result_score_table:
        print_player_result(player_result)

    print('Last frame:')
    frame = replay_data.frame(600)

    print('Players:')
    for player in frame.players:
        print_player(player)

    print('Projectiles')
    for _id, projectile in frame.projectiles.items():
        print_obj(_id, projectile)

    print('Bonuses')
    for _id, bonus in frame.bonuses.items():
        print_obj(_id, bonus)


if __name__ == '__main__':
    main()
